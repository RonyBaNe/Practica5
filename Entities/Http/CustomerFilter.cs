using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Entities.Http
{
    public class CustomerFilter : BaseFilter
    {
        public string? Name {get; set;}

        protected override void SetParameters()
        {
            base.SetParameters();
            _parameters["Name"] = Name;
        }
    }
}