import { observer } from "mobx-react-lite";
import React, { useState } from "react";
import { Toaster, toast } from 'react-hot-toast'

import ICustomer from "../../Entities/customer";
import ICustomerFilter from "../../Entities/http/customerFilter";
import FilterbyState from "../../Entities/http/FilterbyState";
import IState from "../../Entities/state";
import { useStore } from "../../Store/store";




/*interface IProps{
  customers: ICustomer[],
  editCustomerEvent: (customer:ICustomer|null) => void,
}*/


const CustomerList = () => {
  const {customerStore} = useStore()
  const {states ,selectedCustomer} = customerStore

  let defaultState = {
      code: '',
      countryCode: '',
      name: '',
      taxAuthorityCode: '',
  }

  let deafultCustomer = {
      id: 0,
      name: '',
      rfc: '',
      phone: '',
      email: '',
      address: '',
      stateCode: '',
      state: defaultState,
  }

  let customerRef:ICustomer = (selectedCustomer != null)? selectedCustomer: deafultCustomer

  const[customer, setcustomer] = useState<ICustomer>(customerRef)

  const handleCountryStateChange = (event:any) => {
      const {value} = event.target
      setcustomer({...customer, 'stateCode':value})
  }

  const {customers, editCustomerEvent, deleteCustomerEvent, loadCustomer, loadCustomerbystate} = customerStore
  const [filter, setfilter] = useState<ICustomerFilter>({name: ''})
  const [filterstate,  setfilterstate]= useState<FilterbyState>({state: ''})

  const handleInputChanges = (event:any) => {
    const{name, value} = event.target

    setfilter({...filter,[name]:value})
    
  }

  const handleInputChangestate = (event:any) => {
    const{state, value} = event.target

    setfilterstate({...filterstate,[state]:value})

  }


  return (
    <React.Fragment>
      <h3 className="mt-3 mb-3">Customers</h3>

      <div className="row mb-3">
        <div className="input-group">
          <input
              list="carOptions"
              name="name"
              type="text"
              value={filter.name}
              onChange={handleInputChanges}
              className="form-control"
              placeholder="Name or State"
             
            />
              <datalist id="carOptions">
                <option value=" ">All</option>
                <option value="Aguascalientes">Aguascalientes</option>
                <option value="Baja California">Baja California</option>
                <option value="Baja California Sur">Baja California Sur</option>
                <option value="Campeche">Campeche</option>
                <option value="Chiapas">Chiapas</option>
                <option value="Chihuahua">Chihuahua</option>
                <option value="CDMX">Ciudad de México</option>
                <option value="Coahuila">Coahuila</option>
                <option value="Colima">Colima</option>
                <option value="Durango">Durango</option>
                <option value="Estado de México">Estado de México</option>
                <option value="Guanajuato">Guanajuato</option>
                <option value="Guerrero">Guerrero</option>
                <option value="Hidalgo">Hidalgo</option>
                <option value="Jalisco">Jalisco</option>
                <option value="Michoacán">Michoacán</option>
                <option value="Morelos">Morelos</option>
                <option value="Nayarit">Nayarit</option>
                <option value="Nuevo León">Nuevo León</option>
                <option value="Oaxaca">Oaxaca</option>
                <option value="Puebla">Puebla</option>
                <option value="Querétaro">Querétaro</option>
                <option value="Quintana Roo">Quintana Roo</option>
                <option value="San Luis Potosí">San Luis Potosí</option>
                <option value="Sinaloa">Sinaloa</option>
                <option value="Sonora">Sonora</option>
                <option value="Tabasco">Tabasco</option>
                <option value="Tamaulipas">Tamaulipas</option>
                <option value="Tlaxcala">Tlaxcala</option>
                <option value="Veracruz">Veracruz</option>
                <option value="Yucatán">Yucatán</option>
                <option value="Zacatecas">Zacatecas</option>
            </datalist>
              
          
            <button onClick={ () => loadCustomer(filter) } className="btn btn-primary input-group-btn">
              Search
            </button>
        </div>
      </div>
      

      <div className="row mb-3">
            <div className="col-md-12 text-right">
                <button onClick={ () => editCustomerEvent(null) } type="button" className="btn btn-success">
                  New
                </button>
            </div>
        </div>

      <table className="table">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
            <th scope="col">RFC</th>
            <th scope="col">Email</th>
            <th scope="col">State</th>
            <th scope="col">Actions</th>
          </tr>
        </thead>
        <tbody>
          {
           
            customers.map((customer:ICustomer) => (
              <tr key={customer.id}>
                <th scope="row">{customer.id}</th>
                <td>{customer.name}</td>
                <td>{customer.rfc}</td>
                <td>{customer.email}</td>
                <td>{customer.state.name}</td>
                <td>
                  <button onClick={() => editCustomerEvent(customer)} type="button" className="btn btn-primary">
                      Edit
                  </button>
                  &nbsp;&nbsp;
                  <button onClick={() => { toast((t) => (
                    <span>
                      Want to remove this?                      
                      &nbsp;
                      <button onClick={() => deleteCustomerEvent(customer)} type="button" className="btn btn-primary">
                        Delete
                      </button>
                      &nbsp;
                      <button onClick={() => toast.dismiss(t.id)} type="button" className="btn btn-danger">
                        Cancel
                      </button>

                    </span>
                  )); }} type="button" className="btn btn-danger">
                    Delete
                  </button>
                </td>
              </tr>
            ))
          }
          
        </tbody>
      </table>
      <Toaster
      position='top-center'
      reverseOrder={false} />
    </React.Fragment>
    
  );
};

export default observer(CustomerList);
