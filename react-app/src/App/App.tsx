import React from 'react';
import { BrowserRouter, Route, Routes } from "react-router-dom";

import Navigation from './Components/Navigation/Navigation'
import Home from './Containers/Home'
import Customers from './Containers/Customers'
import './Navbar.css'

export default function App(){

    return (
        <BrowserRouter>
            <main className='container'>
                <Navigation/>
                <Routes>
                    <Route path="/" element={<Home/>}></Route>
                    <Route path="/customers" element={<Customers/>}></Route>

                </Routes>

                
            </main>
        </BrowserRouter>
        
    );
}