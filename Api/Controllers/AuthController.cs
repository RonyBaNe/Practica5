using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Api.Services.Interfaces;
using Entities;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers;

[ApiController]

    public class AuthController : ControllerBase
    {

        private readonly IAuthService _authService;
        public AuthController(IAuthService authService)
        {
            _authService = authService;
        }

        [HttpPost]
        [Route("api/[controller]")]
        public async Task<ActionResult<UserSession>> Auth([FromBody] AuthEnt auth)
        {
            var session = new UserSession();

            if (auth == null)
            {
                return BadRequest();
            }

            session = await _authService.GetAuthorization(auth);

            return session;
        }
    }
