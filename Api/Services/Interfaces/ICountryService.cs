using Entities;
namespace Api.Services.Interfaces;

public interface ICountryService
{
    Task<List<State>> GetStateByCountryCodeAsync(string countryCode);
}


